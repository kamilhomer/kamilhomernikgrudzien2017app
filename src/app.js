'use strict'; //to "secure" JS
//var changed to const. Array is not modified after declaration.
const activeModules = [
    { name: 'module 1' },
    { name: 'module 2' },
    { name: 'module 11' },
    { name: 'module 3' },
    { name: 'module 10' }
];
//for loop changed to Array.map
//function() changed to Arrow function

const getHighestNumber = () => 
  Math.max(...activeModules.map(
  // assuming the name convention is always "module_space_number" we can use split function
  highestNumber => highestNumber.name.split(" ")[1]
  ));

let highestNumber = getHighestNumber();    